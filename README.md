  Toms Griblo  
  141REB139  
  REMCO1  
# Teletrafika teorija (RAE555)


## [Kursa "Learning Path: R: Powerful Data Analysis with R" analīze](https://bitbucket.org/TomsGriblo/rae555/wiki/Eks%C4%81mena%20uzdevums)  
## [Uzdevums: Datu lejupielāde no ftp saites](https://bitbucket.org/TomsGriblo/rae555/wiki/Eks%C4%81menauzdevums.md)



## Apskats par padarīto šajā semestrī


### 1.Nedēļa (Nedaudz atkārtojot ko apguvām iepriekšējā kursā)
* What is queuing/ queuing theory?
* Why is it an important tool?   
* Examples of different queuing systems   
* Components of a queuing system   
* The exponential distribution & queuing   
* Stochastic processes   
* Some definitions   
* The Poisson process   
* Terminology and notation
* Little’s formula   

### 2.Nedēļa
*  PRISM and DTMC

### 3.Nedēļa
*  Atskaite par iegūtām prasmēm un iemaņām periodā w1..w3

### 4.Nedēļa
*  Uzdevums, kurā dots DTMC modelis

### 5.Nedēļa
*  Mobile Operator Cell modelis

### 6.Nedēļa
*  Tests

### 7.Nedēļa
*  Midterm Exam Uzdevums
*  10MB izplatīšana Kuba topoloģijas tīklā Uzdevums: Izveidot datu izplatīšanas modeli  Kuba topoloģijas tīklā izmantojot DTMC  
 
  Noskaidrot, ja (BW un Delay ir vienādi visiem savienojumiem) :  
  1. Izveidot DTMC modeli ar PRISM tool palīdzību.  
  2. Cik soļos ir iespējams izplatīt 10MB datus jūsu modelī?  
  3. Aprakstīt  datu izplatīšanas stratēģiju.  
  4. Demonstrēt modelēšanas rezultātus (PDF fails).
  
### 8.Nedēļa  
##### 10MB izplatīšana Kuba topoloģijas tīklā. Advanced model
Ieviest Joslas Platuma parametru (BW) un kavējuma parametru (Delay) katram savienojumam atšķirīgu.

  1. Izveidot MDP modeli ar PRISM tool palīdzību.
  2. Aprakstīt  datu izplatīšanas stratēģiju.
  3. Izvēlēties algoritmu, kas realizē pēc autora domām optimālu, proti, maksimālu datu izplatīšanas ātrumu.
  4. Demonstrēt modelēšanas rezultātus pēc IMMRAD shēmas (PDF fails)  
##### M/M/1:1 modelis
Salīdzināt M/M/1:1 modeli realizētu 3 dažādos veidos:

* Algebraiskais
* Markov Chain
* PRISM

Uzsūtīt piezīmes par modeļu salīdzināšanu pēc:

* P_0
* P_1
* L_sys
* L_q
* L_srv
* W_sys
* W_q
* W_srv

### 9.Nedēļa
* Uzdevums:  
  apgūt tcpdump  
  novērtēt tīkla slodzi izteiktu procentēs  
  
### 10.Nedēļa
* Uzdevums:  
  lietot tcpdump  
  novērtēt tīkla slodzi sadalītu pa trafika grupām (piem: tcp, udp, ack, smtp, ...).  

### 11.Nedēļa
* Uzdevums:  
  lietot tcpdump  


* WEKA
  novērtēt dotā trafika loga parametrus:  
  paku skaitu  
  baitu skaitu  
  komunikācijās iesaistīto mezglu skaitu  
  komunikācijās iesaistīto portu skaitu  
  
### 12.Nedēļa 
* Uzdevums:  
  lietot tcpdump  
  novērtēt sakarību starp videoplūsmu:  
  skaitu  
  pārraides bitrate  
  pārraides kvalitāti  
  
### 13.Nedēļa
* Uzdevums:  
  lietot tcpdump  
  Atkārtot experimentu pēc saites: [links](https://www.slashroot.in/network-traffic-analysis-linux-tools)  
  Iesniegt atskaiti par experimenta rezultātiem  